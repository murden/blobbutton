document.addEventListener("touchstart", function(){}, true);


function watchForHover() {
    var hasHoverClass = false;
    var container = document.body;
    var lastTouchTime = 0;

    function enableHover() {
        // filter emulated events coming from touch events
        if (new Date() - lastTouchTime < 500) return;
        if (hasHoverClass) return;

        container.className += ' hasHover';
        hasHoverClass = true;
    }

    function disableHover() {
        if (!hasHoverClass) return;

        container.className = container.className.replace(' hasHover', '');
        hasHoverClass = false;
    }

    function updateLastTouchTime() {
        lastTouchTime = new Date();
    }

    document.addEventListener('touchstart', updateLastTouchTime, true);
    document.addEventListener('touchstart', disableHover, true);
    document.addEventListener('mousemove', enableHover, true);

    enableHover();
}

watchForHover();

const container = document.getElementById('blurContainer');
const modal = document.getElementById('modal');
const button = document.getElementById('button');
const buttonImg = document.getElementById('buttonImg');
const heading = document.getElementById('heading');
const subheading = document.getElementById('subheading');
const back = document.getElementById('back');


// First we detect the click event on Button
button.addEventListener('click', function () {
	
    setTimeout(function(){ 
    container.classList.add('blured');
    modal.classList.add('z-index-modal');
    modal.classList.add('show');
    }, 250);
    
    setTimeout(function(){ heading.classList.add('show'); }, 650);
    setTimeout(function(){ subheading.classList.add('show'); }, 1950);
    setTimeout(function(){ back.classList.add('show'); }, 3250);
    
});

// First we detect the click event on Button
button.addEventListener('touchstart', function () {
	
    setTimeout(function(){ 
    container.classList.add('blured');
    modal.classList.add('z-index-modal');
    modal.classList.add('show');
    }, 250);
    
    setTimeout(function(){ heading.classList.add('show'); }, 650);
    setTimeout(function(){ subheading.classList.add('show'); }, 1950);
    setTimeout(function(){ back.classList.add('show'); }, 3250);
    
});

// First we detect the click event on Button
buttonImg.addEventListener('click', function () {
	
    setTimeout(function(){ 
    container.classList.add('blured');
    modal.classList.add('z-index-modal');
    modal.classList.add('show');
    }, 250);
    
    setTimeout(function(){ heading.classList.add('show'); }, 650);
    setTimeout(function(){ subheading.classList.add('show'); }, 1950);
    setTimeout(function(){ back.classList.add('show'); }, 3250);
    
});

// First we detect the click event on Button
buttonImg.addEventListener('touchstart', function () {
	
    setTimeout(function(){ 
    container.classList.add('blured');
    modal.classList.add('z-index-modal');
    modal.classList.add('show');
    }, 250);
    
    setTimeout(function(){ heading.classList.add('show'); }, 650);
    setTimeout(function(){ subheading.classList.add('show'); }, 1950);
    setTimeout(function(){ back.classList.add('show'); }, 3250);
    
});

// First we detect the click event on Modal
back.addEventListener('click', function () {
  	
  setTimeout(function(){
   container.classList.remove('blured');
   modal.classList.remove('show');
  }, 250);
   
   
   setTimeout(function(){
   	modal.classList.remove('z-index-modal');
   	heading.classList.remove('show');
   	subheading.classList.remove('show');
   	back.classList.remove('show');
   }, 1050);

});

// First we detect the click event on Modal
back.addEventListener('touchstart', function () {
  	
  setTimeout(function(){
   container.classList.remove('blured');
   modal.classList.remove('show');
  }, 250);
   
   
   setTimeout(function(){
   	modal.classList.remove('z-index-modal');
   	heading.classList.remove('show');
   	subheading.classList.remove('show');
   	back.classList.remove('show');
   }, 1050);

});